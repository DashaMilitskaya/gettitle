package com.example.myapplication2;

import android.os.StrictMode;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


class RetrieveFeedTask{

    private Exception exception;
    private String url;
    private String html_title;

    RetrieveFeedTask(String _url){
        this.url = new String(_url);
    }

    void SetURL(String _url){
        this.url = new String(_url);
    }

    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }

    public String getWEB() {
        HttpURLConnection urlConnection;
        try{
            URL url = new URL(this.url);
            urlConnection = (HttpURLConnection) url.openConnection();

            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String html_t = readStream(in);

                String[] separated = html_t.split("<title>");
                String s = separated[1].split("</title>")[0];

                urlConnection.disconnect();
                return s;

            } catch (IOException e){
                urlConnection.disconnect();
               return null;
            }

        }catch(IOException url) {
           return null;
        }
    }

    protected String executeTask() {
        String s;
        //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        //StrictMode.setThreadPolicy(policy);
        try  {
            s = getWEB();

        } catch (Exception e) {
            s = null;
        }

        return s;

    }

    protected void onPostExecute(String feed) {
        // TODO: check this.exception
        // TODO: do something with the feed
    }
}