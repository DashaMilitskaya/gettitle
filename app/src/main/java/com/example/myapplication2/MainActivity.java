package com.example.myapplication2;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Selection;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {

    RetrieveFeedTask html_task;
    TextView tv;
    String LastString = "Title string";
    EditText urlIn;

    SharedPreferences myPreferences;

    @SuppressLint("MissingInflatedId")
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        myPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        setTitle("Main Activity");
        html_task = new RetrieveFeedTask("http://htmlbook.ru/");

        setContentView(R.layout.activity_main);
        tv = findViewById(R.id.textView2);
        urlIn = findViewById(R.id.editURL);
        if ( savedInstanceState != null ){
            tv.setText(savedInstanceState.getString("tb"));
            LastString = savedInstanceState.getString("tb");
        }
        LastString = myPreferences.getString("URL", "unknown");
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_GET_CONTENT.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                intent.putExtra(Intent.EXTRA_TEXT, LastString);
                setResult(Activity.RESULT_OK, intent);
                finish();

            }
        }
    }
    @Override
    protected void onStart() {
        super.onStart();

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("tb",tv.getText().toString());
    }
    @Override
    protected void onResume(){


        super.onResume();

        /*runOnUiThread(
                new Runnable() {

                    @Override
                    public void run() {
                        tv.setText(html_task.executeTask());
                        setContentView(tv);
                    }
                }
        );*/
        //tv.setText(s);


    }

    public void ButtonClick(View view){

        Thread thread = new Thread(() -> {
            try  {
                html_task.SetURL(String.valueOf(urlIn.getText()));
                String s_title = html_task.executeTask();
                runOnUiThread(
                        () -> {
                            tv.setText(s_title);
                            LastString = s_title;
                            SharedPreferences.Editor myEditor = myPreferences.edit();
                            myEditor.putString("URL", s_title);
                            myEditor.commit();
                        }
                );

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        thread.start();

    }
}